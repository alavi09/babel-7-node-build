class Person {
  #firstname = "Babel";
  #lastname = "JS";

  get #name() {
    return this.#firstname + " " + this.#lastname;
  }

  sayHi() {
    return (`Hi, ${this.#name}!`);
  }
}

const tim = new Person()
console.log(tim.sayHi());
